var _mapStyles = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#a9c4d7"
            },
            {
                "visibility": "on"
            }
        ]
    }
];



$(function(){
    function initialize() {
        var myLatlng = new google.maps.LatLng(56.8293235, 60.57);
        var myOptions = {
            zoom: 14,
            center: myLatlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            options: {
                styles: _mapStyles
            }
        };
        var map = new google.maps.Map(document.getElementById("js-map"), myOptions);
        var contentWindow = '<div style="color: #ff5333; background: #ff5333;" ' +
            'class="b-flag"><div class="b-flag__head">Екатеринбург,<br>ул. 8 марта, 12</div><div ' +
            'class="b-flag__wr"><div class="b-flag__text">24 000м<sup ' +
            'class="b-text-sup">2</sup><br>открытие в 2016 году</div></div></div>';

        var boxText = $(contentWindow)[0];

        var opt = {
            position: new google.maps.LatLng(56.8332,60.5997),
            content: boxText
            ,disableAutoPan: false
            ,maxWidth: 0
            ,pixelOffset: new google.maps.Size(0, 0)
            ,zIndex: null
            ,boxStyle: {
                background: "none"
                ,width: "10px"
            }
            ,isHidden: false
            ,pane: "floatPane"
            ,enableEventPropagation: false
        };


        var ib = new InfoBox(opt);
        ib.open(map);
    }
    
    ($('.b-map').length > 0) && initialize();
});
