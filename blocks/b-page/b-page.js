(function(){
    var timer;
    
    // добавим стили на страницу
    var styl = ".b-page_disable-hover_yes{pointer-events:none !important}";
    $('style').eq(0).before('<style>'+styl+'</style>');
    
    // тут по скролу добавляем класс
    $(window).on('scroll', function() {
        clearTimeout(timer);
        if(!$(body).hasClass('b-page_disable-hover_yes')) {
            $(body).addClass('b-page_disable-hover_yes');
        }

        timer = setTimeout(function(){
            $(body).removeClass('b-page_disable-hover_yes');
        },500);
    }, false);
})();

(function($){
    /*jQuery.extend({ bez: function(coOrdArray) {
        var encodedFuncName = "bez_" + jQuery.makeArray(arguments).join("_").replace(/\./g, "p");
        if (typeof jQuery.easing[encodedFuncName] !== "function") {
            var polyBez = function(p1, p2) {
                var A = [null, null], B = [null, null], C = [null, null],
                    bezCoOrd = function(t, ax) {
                        C[ax] = 3 * p1[ax], B[ax] = 3 * (p2[ax] - p1[ax]) - C[ax], A[ax] = 1 - C[ax] - B[ax];
                        return t * (C[ax] + t * (B[ax] + t * A[ax]));
                    },
                    xDeriv = function(t) {
                        return C[0] + t * (2 * B[0] + 3 * A[0] * t);
                    },
                    xForT = function(t) {
                        var x = t, i = 0, z;
                        while (++i < 14) {
                            z = bezCoOrd(x, 0) - t;
                            if (Math.abs(z) < 1e-3) break;
                            x -= z / xDeriv(x);
                        }
                        return x;
                    };
                return function(t) {
                    return bezCoOrd(xForT(t), 1);
                }
            };
            jQuery.easing[encodedFuncName] = function(x, t, b, c, d) {
                return c * polyBez([coOrdArray[0], coOrdArray[1]], [coOrdArray[2], coOrdArray[3]])(t/d) + b;
            }
        }
        return encodedFuncName;
    }});*/
    
    $(function(){
        var $w = $(window), scrollorama, wh;
        
        if ($('.b-header__background').length != 0 && typeof $.scrollorama == 'function') {
            
            $w.on('resize', function(){
                wh = $w.height();

                if (scrollorama != undefined) {
                    scrollorama.destroy();
                }

                scrollorama = $.scrollorama({
                    blocks:'.b-header',
                    enablePin: false
                });

                scrollorama.animate('.b-header__background',{
                    duration: wh,
                    property:'opacity',
                    delay: 0,
                    start: 1,
                    end: 0
                }).animate('.b-header__background',{
                    duration: wh,
                    property:'scale',
                    delay: 0,
                    start: 1.2,
                    end: 1
                });
            });
        }
        
        $('.b-header__downbtn').on('click', function(){
            $('html, body').animate({
                scrollTop: $('.b-page__main').offset().top - 80
            }, 500);
        });

        var $menu = $('.b-menu'),
            $class= 'b-menu_state_scrolled' +
                ($menu.hasClass('b-menu_theme_inner')?'':' b-menu_theme_inner');
        
        $w.on('scroll', function(){
            if ($(document).scrollTop() > 10) {
                $menu.addClass($class);
            } else {
                $menu.removeClass($class)
            }
        }).trigger('scroll');
        
        $w.trigger('resize');
    });
})(jQuery);