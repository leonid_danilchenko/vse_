(function($){
    $(function(){
        var plugin = (typeof $().owlCarousel) === 'function';
        if ($('.js-gallery__container').length && plugin) {
            var owl1 = $('.js-gallery__container').owlCarousel({
                loop:true,
                margin:2,
                autoWidth:true,
                startPosition: 0,
                nav: true,
                dots: false,
                onInitialized: init
            });
            owl1.on('changed.owl.carousel', eventHandler);
        }


        function eventHandler(event) {
            var item      = event.item.index;
            var pages     = event.item.count;
            nav(item, pages, event.target);
        }

        function nav(i, l, c){
            if (i === l-1) {
                $('.owl-prev', c).removeClass('unactive');
                $('.owl-next', c).addClass('unactive');
            } else if(!i) {
                $('.owl-next', c).removeClass('unactive');
                $('.owl-prev', c).addClass('unactive');
            } else {
                $('.owl-prev, .owl-next', c).removeClass('unactive');
            }
        }

        function init(){
            var item      = this._current;
            var pages     = this._items.length;
            var elem = $(this.$element).find('.owl-nav');
            nav(item, pages, elem);
            
        }

    });
})(jQuery);