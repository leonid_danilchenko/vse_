(function($){
    $(function(){
        $('[type="tel"]').mask("+7 (999) 999 99 99");
        $("#js-form__order").validate({
            
            errorElement: "div",
            messages: {
                name: {
                    required: "Пожалуста, укажите свое имя",
                    minlength: "Длина имени не может быть меньше двух символов"
                },
                tel: {
                    required: "Пожалуйста укажите номер телефона",
                    minlength: "Необходим десятизначный номер"
                }
            },
            submitHandler: function(form) {
                
                //сабмитим  form.submit(); и если успешно то
                
                var url = '#b-modal_type_response';
                modal.closeAll();
                modal.open(url);
                modal.toCenter(url);
            }
        });
    });
})(jQuery);