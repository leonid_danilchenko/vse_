var modal = {};
$(function(){
    modal.open = function(href, e){
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (href.length <= 1) return false;
        $(href+', .b-modal-overlay').show();
    };

    modal.toCenter = function(e){
        var $w = $(e);
        if($w <= 1) return false;
        
        var st = $(window).scrollTop();
        var h = $(window).height();
        var wh = $w.height();
        $w.css({
            'top': wh>h?st:st+(h-wh)/2});
    };
    
    modal.closeAll = function(){
        $('.b-modal, .b-modal-overlay').removeAttr('style');
    };
    
    $('.b-modal__close, .b-modal-overlay, .b-modal, .js-modal__close').on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
        modal.closeAll();
    });

    $(document).on('keyup', function(e){
        if(e.keyCode == '27') {
            modal.closeAll();
        }
    });
    
    $('.b-modal__wrapper').on('click', function(e){
        e.stopPropagation();
    });
    
    $('.js-modal').on('click', function(e){
        modal.closeAll();
        var _href = $(this).attr('href');
        modal.open(_href,e);
        modal.toCenter(_href);
    });
});