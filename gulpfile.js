'use strict';

const gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    resolv= require('stylus').resolver,
    gconcat = require('gulp-concat'),
    debug = require('gulp-debug'),
    cached = require('gulp-cached'),
    nib = require('nib'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    remember = require('gulp-remember'),
    gulpIf = require('gulp-if'),
    jstransformer = require('jstransformer'),
    jstransformerStylus = require('jstransformer-stylus'),
//    csso = require('gulp-csso'),
    del = require('del'),
    path = require('path'),
    newer = require('gulp-newer'),
    htmlPrettify = require('gulp-prettify'),

    gulpJade = require('gulp-jade'),
    jade = require('jade'),
    data = require('gulp-data'),
    
    notify = require('gulp-notify'),
//    jadeInherit = require('gulp-jade-inheritance'),
    browserSync = require('browser-sync').create(),
    plumber = require('gulp-plumber');
    
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

function getData (file) {
    var dataEntry;
    var data;
    var fs = require('fs');

    try {
        dataEntry = fs.readFileSync(file, 'utf8');
    } catch (er) {
        dataEntry = false;
    }

    if (dataEntry) {
        eval('data = {' + dataEntry + '}');
    } else {
        data = '{}';
    }

    return data;
};

gulp.task('stylus', function(){
    return gulp.src([
            'blocks/i-fonts/*.styl',
            'blocks/i-reset/*.styl',
            'blocks/_b-*/*.styl',
            'blocks/b-*/*.styl',
            'blocks/-b-*/*.styl'])
        .pipe(gulpIf(isDevelopment, debug({title: 'src'})))
        .pipe(plumber({
            errorHandler: notify.onError(function(err){
                return {
                    title: 'Stylus',
                    message: err.message
                }
            })
        }))
//        .pipe(debug({title: 'plumber'}))
        .pipe(gulpIf(isDevelopment, sourcemaps.init({loadMaps: true})))
//        .pipe(debug({title: 'sourcemaps_init'}))
        .pipe(cached('styles'))
//        .pipe(debug({title: 'cached'}))
        .pipe(stylus({
            use:[nib()], 
            import: ['../config.styl', '../i-mixins/i-mixins__my.styl']
        })) // собираем stylus
//        .pipe(debug({title: 'stylus'}))
        .pipe(autoprefixer({ browsers: ['> 2%','last 2 versions', 'IE 9'], cascade: false }))
//        .pipe(debug({title: 'autoprefixer'}))
        .pipe(remember('styles'))
//        .pipe(debug({title: 'remember'}))
        .pipe(gconcat('style.css')) // собираем stylus
//        .pipe(debug({title: 'gconcat'}))
//        .pipe(csso())
//        .pipe(debug({title: 'csso'}))
        .pipe(gulpIf(isDevelopment, sourcemaps.write('.', {addComment: true, includeContent: false})))
//        .pipe(debug({title: 'sourcemaps_write'}))
        .pipe(gulp.dest('./publish'));
    
});

jade.filters.stylus = jstransformer(jstransformerStylus);
jade.filters.shoutFilter = function (str) {
    return str + '!!!!';
};

gulp.task('combine-data', function (cb) {
    return gulp.src(['blocks/**/*.json', '!**/_*.json'])
        .pipe(plumber({
            errorHandler: notify.onError(function(err){
                return {
                    title: 'Data',
                    message: err.message
                }
            })
        }))
        .pipe(gconcat('data.json', { newLine: ',\n\n' }))
        .pipe(gulp.dest('tmp'));
});

gulp.task('jade', function() {         //плохо сделано :(
    return gulp.src(['jade/*.jade', '!jade/layout/*.jade'])
        .pipe(plumber({
            errorHandler: notify.onError(function(err){
                return {
                    title: 'Stylus',
                    message: err.message
                }
            })
        }))
        .pipe(data(getData('tmp/data.json')))
//        .pipe(cached('jade'))
        .pipe(gulpJade({
            jade: jade,
            pretty: '\t'
        }))
//        .pipe(remember('jade'))
        .pipe(gulp.dest('./')); // Записываем собранные файлы
});


gulp.task('js', function() {
    return gulp.src(['lib/**/*.js','!lib/html5shiv/*.js','!lib/jquery/*.js','!lib/google.map/*.js','blocks/**/*.js'])
        .pipe(gconcat('script.js'))
        .pipe(gulp.dest('publish'))
});

gulp.task('clean', function(){
    return del(['./publish']);
});     

gulp.task('assets', function(){
    return gulp.src(['blocks/**/*.{png,svg,gif,jpg,jpeg}',
            'blocks/i-fonts/**/*.{woff,woff2,ttf,eot,otf}',
            'blocks/_b-rub/**/*.{woff,woff2,ttf,eot,otf}'],{since: gulp.lastRun('assets')})
        .pipe(newer('publish'))
        .on('data', function(file){
            switch (file.extname) {
                case '.png':
                case '.svg':
                case '.gif':
                case '.jpg':
                case '.jpeg':
                    file.path = file.base + file.basename;
            }
        })
        .pipe(debug({title:'assets'}))
        .pipe(gulp.dest('publish'));
});

gulp.task('build', gulp.series(
    'clean',
    'combine-data',
    gulp.parallel('assets','stylus', 'js', 'jade'))
);

gulp.task('watch', function(){
    gulp.watch(['blocks/**/*.styl'], gulp.series('stylus'))
        .on('unlink', function(filepath){
            var file = path.parse(filepath);
            remember.forget('stylus', path.resolve(file.dir+'/'+file.name+'.css')); //?
            remember.forget('stylus', path.resolve(filepath)); //?
            
            delete cached.caches.styles[path.resolve(filepath)];
            delete cached.caches.styles[path.resolve(file.dir+'/'+file.name+'.css')];
        });
    gulp.watch('blocks/**/*.{png,gif,jpg,jpeg,woff,woff2,ttf,svg,eot,otf}', gulp.series('assets'));
    gulp.watch(['jade/**/*.jade','blocks/**/*.jade'], gulp.series('jade'));
    gulp.watch(['lib/**/*.js','blocks/**/*.js'], gulp.series('js'));
    gulp.watch(['blocks/**/*.json'], gulp.series('combine-data', 'jade'));
});

gulp.task('serve', function(){
    browserSync.init({
        server: './'
    });

    browserSync.watch(['publish/**/*.*', './*.*', 'tmp/**/*.*']).on('change', browserSync.reload);
});

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));