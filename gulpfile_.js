// Инициализируем плагины
var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS         //nano?
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    gconcat = require('gulp-concat'), // Склейка файлов
    connect = require('gulp-connect'), // Webserver
    server = lr(),
    nib = require('nib'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cached = require('gulp-cached'),
    remember = require('gulp-remember'),
    plumber = require('gulp-plumber');

//sprite, svg, base64 ???

/*gulp.task('assemble', function () {
    gulp.src('pages*//*.hbs')
        .pipe(gulpAssemble(assemble))
        .pipe(extname())
        .pipe(gulp.dest('./'));
});*/

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src([
        './blocks/i-reset/*.styl',
        './blocks/_b-*/*.styl',
        './blocks/b-*/*.styl',
        './blocks/-b-*/*.styl'])
        .pipe(plumber())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(cached('styles'))
        .pipe(stylus({use:[nib()], import: ['../config.styl', '../i-mixins/i-mixins__my.styl']})) // собираем stylus
        .pipe(autoprefixer({ browsers: ['> 2%','last 2 versions', 'IE 9'], cascade: false }))
        .pipe(remember('styles'))
        .pipe(gconcat('style.css')) // собираем stylus
        .pipe(csso())
        .pipe(sourcemaps.write('./publish', {addComment: true, includeContent: false}))
        .pipe(gulp.dest('./publish'));
});



// Собираем html из Jade

gulp.task('jade', function() {
    gulp.src(['./jade/*.jade', '!./jade/layout/*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('./')) // Записываем собранные файлы
    .pipe(livereload(server)); // даем команду на перезагрузку страницы
}); 



// Собираем JS
gulp.task('js', function() {
    gulp.src(['./lib/**/*.js','./blocks/**/*.js'])
        .pipe(concat('script.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./publish'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});



// Копируем и минимизируем изображения

gulp.task('images', function() {
    gulp.src('./blocks/**/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./publish/img'))

});



// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(connect.static('./'))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});

gulp.task('connect', function () {
    connect.server({
        root: ['./'],
        port: 8000,
        livereload: true
    });
});

// Запуск сервера разработки gulp watch
gulp.task('default', function() {
    // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('jade');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('blocks/**/*.styl', function() {
            gulp.run('stylus');
        });
        gulp.watch('jade/**/*.jade', function() {
            gulp.run('jade');
        });
        gulp.watch('blocks/**/img/*', function() {
            gulp.run('images');
        });
        gulp.watch(['./blocks/**/*.js', './lib/**/*.js'], function() {
            gulp.run('js');
        });
    });
    gulp.run('connect');
});

gulp.task('prod', function() {
    // css
    gulp.src('./blocks/**/*.styl')
        .pipe(stylus({
            use: ['nib']
        })) // собираем stylus
    .pipe(myth()) // добавляем префиксы - http://www.myth.io/
    .pipe(csso()) // минимизируем css
    .pipe(gulp.dest('./publish/')); // записываем css

    // jade
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade())
        .pipe(gulp.dest('./publish/'));

    // js
    gulp.src(['./blocks/**/*.js', './lib/**/*.js'])
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./publish'));

    // image
    gulp.src('./blocks/**/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./publish/img'));
    
    
    
});